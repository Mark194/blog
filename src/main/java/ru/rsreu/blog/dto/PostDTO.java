package ru.rsreu.blog.dto;


import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostDTO {
    private String title;
    private String body;
    private String image;
}
