package ru.rsreu.blog.service.api;

import ru.rsreu.blog.dto.PostDTO;

import java.util.List;

public interface PostService {
    public List<PostDTO> search(String query);
}
