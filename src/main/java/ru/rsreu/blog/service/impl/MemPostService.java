package ru.rsreu.blog.service.impl;


import org.springframework.stereotype.Service;
import ru.rsreu.blog.dto.PostDTO;
import ru.rsreu.blog.service.api.PostService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MemPostService implements PostService {

    private ArrayList<PostDTO> posts = new ArrayList<>(Arrays.asList(
            PostDTO.builder()
                    .title("First title")
                    .body("First body")
                    .image("/img/1.png")
                    .build(),
            PostDTO.builder()
                    .title("Second title")
                    .body("Second body")
                    .image("/img/2.jpg")
                    .build(),
            PostDTO.builder()
                    .title("Third title")
                    .body("Third body")
                    .image("/img/3.png")
                    .build()));

    @Override
    public List<PostDTO> search(String query) {
        return query != null && !query.isEmpty() ? posts.stream().filter(post ->
                post.getTitle().toLowerCase().matches(".*"+query.toLowerCase()+".*"))
                .collect(Collectors.toList())
                : posts;
    }
}
