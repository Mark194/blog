package ru.rsreu.blog.service.impl;

import org.springframework.stereotype.Service;
import ru.rsreu.blog.domain.Post;
import ru.rsreu.blog.dto.PostDTO;
import ru.rsreu.blog.jpa.PostRepository;
import ru.rsreu.blog.service.api.PostService;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DbPostService implements PostService {

    private final PostRepository postRepository;

    public DbPostService(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public List<PostDTO> search(String query) {
        List <Post> posts = query != null && !query.isEmpty()?
                postRepository.findByTitleLike("%"+query+"%")
                : postRepository.findAll();
        return posts.stream().map(post -> PostDTO.builder()
                .title(post.getTitle())
                .body(post.getBody())
                .image(post.getImage())
                .build())
                .collect(Collectors.toList()
                );
    }

//    @PostConstruct
//    public void setup(){
//        postRepository.saveAll(Arrays.asList(
//                Post.builder()
//                        .title("First title")
//                        .body("First body")
//                        .image("/img/1.png")
//                        .build(),
//                Post.builder()
//                        .title("Second title")
//                        .body("Second body")
//                        .image("/img/2.jpg")
//                        .build(),
//                Post.builder()
//                        .title("Third title")
//                        .body("Third body")
//                        .image("/img/3.png")
//                        .build()));
//    }
}
