<#macro main>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Blog</title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="/css/materialize.min.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

</head>
<body>

<nav>
    <div class="nav-wrapper">
        <a href="#" class="brand-logo center">Blog</a>
        <ul id="nav-mobile" class="left hide-on-med-and-down">
            <li>
                <nav>
                    <div class="nav-wrapper">
                        <form>
                            <div class="input-field">
                                <input name="query" id="search" type="search" required>
                                <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                                <i class="material-icons">close</i>
                            </div>
                        </form>
                    </div>
                </nav>
            </li>
            <li>
                <a href="/"><i class="material-icons">cancel</i></a>
            </li>
        </ul>
    </div>
</nav>

    <#nested>
<script type="text/javascript" src="/js/materialize.min.js"></script>

</body>
</html>
</#macro>